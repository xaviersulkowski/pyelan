import numpy as np


class ElanAnnotations(object):

    def __new__(cls, eaf_file):
        return object.__new__(cls)

    def __init__(self, eaf_file):
        self.eaf = eaf_file
        self.eaf = self.__open_eaf_file()

    def __open_eaf_file(self):
        return open(self.eaf, 'r').read()

    def __get_timestamp(self, file):

        start_word = 'TIME_VALUE'
        annotation_time_start = file.find('"', file.find(start_word))
        annotation_time_end = file.find('"/', annotation_time_start)
        time_annotation = file[annotation_time_start + 1:annotation_time_end]

        return time_annotation, annotation_time_end

    def __get_anotation_value(self, file):

        start_word = 'ANNOTATION_VALUE'
        annotation_value_start = file.find('>', file.find(start_word))
        annotation_value_end = file.find('<', annotation_value_start)
        annotation_value = file[annotation_value_start + 1:annotation_value_end]

        return annotation_value, annotation_value_end

    def amount_of_annotations(self):

        start_word = 'lastUsedAnnotationId'
        used_annotations_start = self.eaf.find('>', self.eaf.find(start_word))
        used_annotations_end = self.eaf.find('<', used_annotations_start)
        amount = self.eaf[used_annotations_start + 1:used_annotations_end]

        return amount

    def get_all_timestamps(self):

        copy = self.eaf
        start = []
        stop = []

        for i in range(int(self.amount_of_annotations()) * 2):
            time_annotation, end_position = self.__get_timestamp(copy)
            if i % 2 == 0:
                start.append(int(time_annotation) / 1000)
            else:
                stop.append(int(time_annotation) / 1000)
            copy = copy[end_position:]

        return start, stop

    def get_all_values(self):

        copy = self.eaf

        annotations_values = []
        for i in range(int(self.amount_of_annotations())):
            annotation_value, end_position = self.__get_anotation_value(copy)
            try:
                annotations_values.append(int(annotation_value))
            except:
                annotations_values.append(1)
            copy = copy[end_position + len('ANNOTATION_VALUE'):]

        return annotations_values

    def complete_annotations(self, signal_length, samplerate):

        starts, stops = self.get_all_timestamps()
        values = self.get_all_values()

        starts = [int(i * samplerate) for i in starts]
        stops = [int(i*samplerate) for i in stops]

        annotations = np.zeros(signal_length)
        for i in range(len(starts)):
            for j in range(len(annotations)):
                if j == starts[i] - 1:
                    for k in range(stops[i] - starts[i]):
                        annotations[j+k] = values[i]
                else:
                    pass

        return starts, stops, annotations
